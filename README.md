# AlgoMentor #

This is a pre-alpha public release. 

Builds are nightly!

# Status ( 8/28/2014 )

* File sync is broken, New build up soon with
* * Patched ftp
* * google and dropbox OAuth works, fixing low level sockets for file download

* Data Analysis algo
* * Speed improvements, new python -> cython workflow
* * Improved template schema


* Questions interface
* * Creating templates gui page, json here: http://app1.virtuhorizon.com/secuapp-templatesjs.php


* Encryption
* * AES implementation -> cython (^ speed ^)
* * Optional keyfile export
* * Hardening cross platform entropy generation


# Where Can I Get It?
Simply download the right package for your device and install!

* [![Alt text](http://dev.virtuhorizon.com/wp-content/uploads/2014/07/icon-android.png)](http://code.virtuhorizon.com/app-public/downloads/fluidSec.apk)


All other platform builds are scheduled to be released once alpha is finished ( I don't want to pay apple yet! )